## Gateway Configuration
***
Gateway configuration with **Chirpstack** is essential part on successful **LoRaWAN** transmission.
Proper gateway configuration is essential for efficient communication between end devices and the ChirpStack LoRaWAN network, ensuring reliable and accurate data transmission within the Internet of Things (IoT) ecosystem.

There are two crucial part we need to do for configuring gateway with Chirpstack,
* Configure with **Dragino** user interface.
* Configure with installed **Chirpstack**.

## Gateway configuration with **Dragino User Interface**.
***

* We need to select **Gateway** as per our requirement
* Here we choose **DLOS8 Outdoor LoRaWAN Gateway** You can refer the product specifications from the link mentioned here "https://www.dragino.com/products/lora-lorawan-gateway/item/160-dlos8.html".
* There are two possible ways to connect our **Dragino DLOS8**gateway with **Dragino** user interface,
	* Using cellular connection
	* Using LAN
* Here we are using LAN ethernet connection.

These are the steps we need to do within **Dragino User Interface**.
 
1. The first step is, power on our gateway with a power supply with a adapter.
2. Connect LAN cable with one side on  source LAN connector and other side on corresponding  LAN port in our Gateway.
3. There are some LED indications for understanding our current gateway status. 
4. These are the indications,
	* SOLID GREEN: DLOS8N is alive with LoRaWAN server connection.
	* BLINKING GREEN: 
		* Device has internet connection but no LoRaWAN Connection.
		* Device is in booting stage, in this stage, it will BLINKING GREEN for several seconds and then RED and YELLOW will blink together.
		* SOLID RED: Device doesn't have Internet connection.
5. Access and Configure DLOS8N
	* The DLOS8N is configured as a WiFi Access Point by default. User can access and configure the DLOS8N after connecting to its WiFi network, or via its Ethernet port.
	* Here we are using Ethernet port.
6. After successful connection through ethernet, go to this link (http://10.130.1.1/cgi-bin/home.has) where we can give configure settings as per our requirement.

Below are the steps that we need to do under **Dragino** User Interface they are,

* Under LoRa category select in dropdown frequency plan as **IN865 India 865MHz(865-867)**

   ![alt text](https://gitlab.com/Makeshm/chirpstack_v4/-/raw/main/images/Dragino_Lora_UI.png?ref_type=heads)

	* Select **save & apply**

* Under LoRaWAN category, under the dropdown select **Service Provider** as **Custom/Private LoRaWAN**.
![alt text](https://gitlab.com/Makeshm/chirpstack_v4/-/raw/main/images/Dragino_Lorawan.png?ref_type=heads)
* Enter Server Address as ip address of chirpstack version 4 installed PC.
	* Select **save & apply**

* And click on **Home** on right side where we can be able to see the current status of our dragino dlos8 gateway current status interms of Internet, IOT service, LoRa, WiFi access point.

This is how it looks like after successfull gateway configuration with **Dragino User Interface**,

![alt text](https://gitlab.com/Makeshm/chirpstack_v4/-/raw/main/images/Gateway_Dragino_interface.png?ref_type=heads)




## Gateway Configuration with Chirpstack
***

1. Click on subsection "Gateways" under "Tenant".

![alt text](https://gitlab.com/Makeshm/chirpstack_v4/-/raw/main/images/Gateway_navigation.png?ref_type=heads)

2. Click on Click on "Add gateway"

![alt text](https://gitlab.com/Makeshm/chirpstack_v4/-/raw/main/images/Add_Gateway_1st_step.png?ref_type=heads)

3. There shows three subsection **General**, **Tags** and **Metadata**.

![alt text](https://gitlab.com/Makeshm/chirpstack_v4/-/raw/main/images/Adding%20gateway.png?ref_type=heads)
![alt text](https://gitlab.com/Makeshm/chirpstack_v4/-/raw/main/images/Adding_gateway_01.png?ref_type=heads)

4. Under **General** Category,
5. Enter name of the gateway under **Name** tab.
6. Enter the description of this Gateway under **Description** tab.
7. Enter gateway ID under **Gateway ID (EUI64)** tab, where we can generate seperate ID for this gateway configuration or the ID shown in our gateway device.
8. In **Stats interval (secs)** input field, were the expected interval in seconds in which the gateway sends its live statistics.
	* click on **submit**
9. Under **Tags** section we can be able to add tags in **Add tab** subsection as key value pair.
	* click on **submit**.
10. In **Metadata** section it is stating that "Metadata is pushed by the gateway on every stats update and can be used to expose information about the gateway like ip / hostname, serial number, HAL version."
