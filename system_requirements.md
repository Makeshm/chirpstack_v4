## Version Requirements for Chirpstack version 4 installation
***

* Chirpstack Version need to be downloaded is : "chirpstack 4.6.0".
* Postgres Requirement : Chirpstack version 4 supports PostgreSQL version 9.5 and above.
* Chirpstack Gateway Bridge Requirement : It supports "v3.14.0+ version". 
    * Need to download : "4.0.10".
* Chirpstack application server requirement : Need to download "3.17.9 version".
* Chirpstack network server requirement : Need to download "3.16.8 version".
* MQTT version needed : 3.1.1+ version.

## System Requirements
***

General recommendations for the minimum system requirements for running ChirpStack :

* CPU: A multi-core processor is recommended, with the exact requirements depending on the expected number of devices and messages.
* RAM: ChirpStack components may benefit from having sufficient RAM, especially if you're handling a large number of devices. A minimum of 2 GB or more is generally recommended.
* Storage: The storage requirements depend on factors such as the size of your device data, logs, and database. Ensure you have enough disk space to accommodate these needs. A few gigabytes of free space should be considered a minimum.
* Operating System: ChirpStack is typically platform-independent and can run on various operating systems, including Linux. Ubuntu is a commonly used distribution, but others like CentOS or Debian are also suitable.
* Database: ChirpStack uses a PostgreSQL database to store network and application data. Ensure that your PostgreSQL installation meets the requirements for the expected workload.
* Network Connectivity: Reliable network connectivity is crucial, especially if you are working with LoRaWAN gateways and devices. Ensure that your server has a stable internet connection.

