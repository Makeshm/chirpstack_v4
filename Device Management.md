## Device Management
***


A device is the end-device connecting to, and communicating over the LoRaWAN network. Device management in ChirpStack refers to the set of activities and functionalities related to the configuration, monitoring, and control of LoRaWAN devices within a LoRaWAN network. ChirpStack is an open-source LoRaWAN network server that facilitates communication between LoRaWAN devices (end devices) and application servers. Device management is essential for maintaining a well-functioning and secure LoRaWAN network.

Device management involves some important aspects they are :-

*  Adding and registering devices
* Device profiles and settings
* Device status and statistics

### Adding and registering devices

This process is essential for integrating devices into the LoRaWAN network, allowing them to communicate with the network server and, ultimately, with the associated application servers.

For registering new devices , intialy we ned to create application for the device, then add the device and device profile settings.

Let's look on to How we can add and register a new device. This include some steps:

 __Step 1 :__ We need to create an application for the device.For that,
    
* Login to ChirpStack webserver (_localhost:8080_) and navigate to  > Tenants > Application > Add Application.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Application.png?ref_type=heads)

* There displays a window for the creating application with name and description as requirements.Also another section named tags.

    __Tags :__ Each device can have multiple user-defined tags.Tags are exposed when ChirpStack published device events and can be used to add additional meta-data.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Add_Application.png?ref_type=heads)

* After providing the necessary name and description for the application then click submit and the application for the device is created successfully.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Application_Created.png?ref_type=heads)

__Step 2 :__ Now we can add the device on to the application.

* After creating the application there display window (see the above fig). On the same window tap into Devices > Add device.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Add_device.png?ref_type=heads)

* Provide the __Name__ and __Description__ for the device.
* __Device EUI:__ A globally unique identifier for the device. It is a fundamental parameter that uniquely identifies the device in the LoRaWAN network.The EUI is a 64-bit value, typically represented as an 8-byte hexadecimal number. The bytes in the EUI can be transmitted in different orders: __Most Significant Byte (MSB)__ first or __Least Significant Byte (LSB)__ first. The choice of MSB or LSB depends on the network and application requirements.

* __Join EUI :__ The __Join EUI__ will be automatically set / updated on OTAA. However, in some cases this field must be configured before OTAA (e.g. OTAA using a Relay).

* __Device profile__: Device profile is another mandotory requement inorder to create a device.
The device profile specifies the capabilities, configurations, and constraints of the device, such as supported data rates, channels, and additional settings.

    So inorder to create a device, device profile also should be created. (refer below - how to create the device profile - __Device Profile__)

* __Device is disabled :__ When disabled, Received uplink frames and join-requests will be ignored.

* __Disable frame-counter validation :__ Frame counter validation is a security mechanism in LoRaWAN to protect against replay attacks and ensure the integrity of transmitted messages.You must re-activate your device before this setting becomes effective. Note that disabling the frame-counter validation will compromise security as it allows replay-attacks.















