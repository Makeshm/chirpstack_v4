## Device Profile 
***

Device profile is a configuration method that is used to define the purpose and behavior of a specific type of LoRaWAN device. And also the device profile serves as standardized set of parameters that give us various methods in terms for device activation and device encoding and decoding etc.

## Device Profile Components:

* *Communication Parameters* 
* *Activation Methods*  
* *Security Settings* 
* *Data Payload Format* 
* *Frame Counter Restrictions* 
* *Class Type* 

## Creating a Device Profile
***

In order to create a new device profile:

* Access the "Device Profiles" section.

    ![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Device_Profile_navigation.png?ref_type=heads)

* After entering Device Profiles section Click on "Add Device Profile".

    ![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Creating_device_profile_1st_step.png?ref_type=heads)

* After entering Add Device Profile tab we need to provide some essential details about device profile under **General** category,

    ![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Creating_Device_Profile_general.png?ref_type=heads)
    
    ![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Device_Profile_creation_general.png?ref_type=heads)

    * Device Name and Purpose in **Name** and **Desciption** tab.
    * Under which regional parameters our device should work we should mention it under a dropdown **Region**

    ![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Creating_Device_Profile_Region_Selection.png?ref_type=heads)

    * There's another dropdown **Region Configuration** *By selecting a region configuration, the device will only work within the selected region configuration. If left blank, the device will work under all region configurations of the selected region.*
    
         If we select **IN8658** as **Region** it will show only **IN865** as **Region Configuration**.
         If we select **US915** as **Region** then it there will be a option to select which **Region Cofnfiguration** need to be selected. So we need to assign both **Region** and **Region Configuration** parameters according to our suitablity.
    
    * There's another dropdown **MAC Version** where we need to select suitable LoRaWAN MAC version for our purpose select suitable LoRaWAN MAC versions starting from (1.0.0 to 1.1.0).

    * Click on dropdown **Regional Parameters Revision** It is used for both of our system and regional parameter requirements, for a device.
If we are using class-A devices for **LoRaWAN communication** then we need to select **class - A**.
    
    * Another dropdown **ADR algorithm** This dropdown is used to set at which data rate a device must communicate with the corresponding network.

        If we select **Default ADR algorithm (LoRa only)** ADR, in the context of LPWAN, refers to the capability of the network server to dynamically adjust the communication parameters of a device. This includes adjusting data rate, transmit power, and other parameters to optimize communication performance.

        Other two dropdown is **LR-FHSS only ADR algorithm** and **LoRa & LR-FHSS ADR algorithm** these both dropdown will introduce "Low Range Frequency Hopping Spread Spectrum" for controlling device data rate.
    
    * Another toggle switch, **Flush queue on Activate** when the device is being activated (or reactivated), any data that is currently in the uplink queue will be removed or cleared. 
        
        This option is used to make ensure that the device starts with a clean state regarding the data it needs to send to the network after activation.
    
    * Set this input field **Expected uplink interval(secs)**, as 3000. 
    
        The expected interval in seconds in which the device sends uplink messages. This is used to determine if a device is active or inactive".

    * Another toggle switch **Allow Roaming** whether we need to allow roaming or not. 

        Related to our server configuration.
    * The input field **Device-status request frequency (req/day)** where we need to enter number of times a device sends a status request to the network server within a day.
        
        So it's based on our decision on what frequency we want to know the status of the device, if we enter zero states that we don't want to know device status per day. 
    
    * Click on **Submit** button to save all the details that we entered under "General" category for **Device Profile** creation.
    



    



