## Tenent management
***
* Tenant management, in the context of ChirpStack, typically refers to the ability to manage multiple tenants or organizations on the same ChirpStack instance. 

* Each tenant may have its own set of devices, applications, and network settings.

* Now we are looking at, how we can add a tenant in ChirpStack and set permissions to it.

* __Steps:__

__1.__ Navigate to the __Tenant__ tab inside the main __Network Server__ tab. There we see the list of registered tenants and a button __add tenant__ for creating new tenants.

    
![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Tenent_Creation.png?ref_type=heads) 

__2.__ After clicking __add tenant__ there displays a window like this.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Add_tenant.png?ref_type=heads)

 Here we can give _Name_ and _description_ for your tenant and there are other features which is indicated in the picture which includes features like _tenant can have gateways_, _Gateways are private(uplink)_, _Gateways are private(downlink)_,_Max Gateway Count_, _Max device Count_ and then click submit and your tenant is been created

* __tenant can have gateways__: When enabled  the tenant can add gateways.But the usage of the gateways is not limited to this tenant only unless these are marked private.

* __Gateways are private (uplink)__: Uplink received by gateways of this tenant can only be used by the devices of this tenant.

*  __Gateways are private (downlink)__:Other tenants can not use the gateways of this tenant for downlinks. This can be useful in case uplinks are shared with other tenants, but you want to avoid other tenants using downlink airtime of your gateways.

* __Max. gateway count__ & __Max. device count__:
It is the maximum number of gateways that can be added by this tenant and the maximum number of devices that can be added by this tenant respectively.

__3.__ You can navigate to newly created tenant through the dropdown menu or you can navigate to dashboard inside the Tenant main menu which displays the details of the newly created tenant.
![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Dashboard.png?ref_type=heads)

* __Dashboard__ provides the details of Active devices, Active gateways, Device data-rate usage, Gateway map and there is also a __Configuration__ tab inside the dashboard.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Dashboard_configuration.png?ref_type=heads)

Same configuration had been done before, but here we can edit and update it.For example if we want to increase the number of gateway or devices or set the Gateways to public etc can be updated as shown above. 

 Also there is a button for Delete tenant for deleting the tenant.

### Tenant User management

Like the User management we done before in the ChirpStack Network server there is also a tab for __Users__ inside the tenant.Here we can create new user management features for authentication and authorization.

A Tenant can have so many users, but a tenant can add users only if the user is registered in the Network server.

So Let's create a new tenant user which includes some steps.

* Intially create an user inside Network server for that tap into __"Users"__ and __"Add User"__.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Tenant_Users.png?ref_type=heads)

* Fill up all the required details and set the permissions then click __Submit__.

    * __Is active__: If the __Is active__ is enabled that means the created user is a live user and the admin can turn it off if the user is no longer a participant.
    * __Is admin__: If enabled the created user will get same privileges as the default admin.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Tenant_demoCreat.png?ref_type=heads)

* And new user is created succesfully.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/demouser_icfoss.png?ref_type=heads)

* Now we can add tenant user with the same user we created.Tap on to __Users__ inside the Tenant main menu and user window will be displayed. Tap into __"Add Tenant User"__ to create new user inside the tenant.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Tenent_in_user.png?ref_type=heads)

* After that a new window will be displayed for the Tenant User creation.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/Tenant_in_add.png?ref_type=heads)

* Here we have to provide the _Email (of existing user)_ and permissions like _User is tenant admin_, _User is gateway admin_, _User is device admin_.
   
    * __Email (of existing user)__: A user without additional permissions will have the capability to view all resources within this tenant and engage in the sending and receiving of device payloads.
    * __User is tenant admin__: A tenant admin user is able to add and modify resources part of the tenant.
    * __User is gateway admin__: A gateway admin user is able to add and modify gateways part of the tenant.
    * __User is device admin__: A device admin user is able to add and modify resources part of the tenant that are related to devices.

* After submitting the details and permissions the tenant user is created successfully as shown below.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/tenant_in_created_.png?ref_type=heads)






