# ChirpStack API
***

ChirpStack provides an API (Application Programming Interface) that allows developers to interact with and control various aspects of the ChirpStack system. The ChirpStack API is essential for managing LoRaWAN networks, devices, and gateways. The ChirpStack API provides a range of features for managing and interacting with LoRaWAN networks. Here are some key features of the ChirpStack API:

### Features
***
* Device Management
* Application Management
* Gateway Management
* Network Monitoring
* Device Activation and Configuration
* User and Organization Management


# Authentication and Authorization:
***

* Before making requests to the ChirpStack API, authentication is typically required. 
* This is often done using API tokens or keys. 
* The authentication process ensures that only authorized users or applications can access and manipulate data within ChirpStack.

### Complete Authorization Over all the tenants
***

* To get the complete authorization over all the tenants(organizations), Go to the "Network Server" on the left side. Click on "API Keys".

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img11.png)

* Click "Add API Key" on the top right corner.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img12.png?ref_type=heads)

* Enter any name and click submit to create an API key.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img13.png?ref_type=heads)

* Copy the obtained API Key.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img14.png?ref_type=heads)

* Now open a new tab and enter localhost:8090 or replace it with your own domain name to access ChirpStack REST API

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img15.png?ref_type=heads)

* Click on "Authorise" on top right corner

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img16.png?ref_type=heads)

* Now paste the API key copied from the ChirpStack Application Server and click submit. You need to enter as follows:

'Bearer token'

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img17.png?ref_type=heads)

* Now we need to enter the tenandID(organisation ID) for getting the data regarding that specific tenant. For that we need to go back to the application server and follow the instructions below.

* Click on the tenant that we need to access.
![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img18.png?ref_type=heads)

* Copy the tenant ID as follow
![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img19.png?ref_type=heads)

* Then paste it in the Tenant ID field inside the Application service in ChirpStack REST API Server and click execute.
![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img20.png?ref_type=heads)

* Scroll down to see the Complete application data inside that specific tenant


### Authorization over a specific tenant
***

* To get authorization over a specific tenant, go to the "Tenants" section on the left side and click on "API Keys".

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img21.png)

* Click "Add API Key" on the top corner.

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img22.png)

* Follow all the same steps done before:

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img23.png)

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img24.png)

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img16.png)

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img17.png)

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img18.png)

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img19.png)

![alt text](https://gitlab.com/icfoss1434642/chirpstack_v4/-/raw/main/images/img20.png)





