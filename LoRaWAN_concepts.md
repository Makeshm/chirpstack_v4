## LoRaWAN

***
* **LoRaWAN**, which stands for Long Range Wide Area Network, is a wireless communication protocol designed for long-range communication between devices in the Internet of Things (IoT). 

* It enables low-power, wide-area communication between devices, making it suitable for applications such as smart cities, agriculture, industrial automation, and more.

#### Here are some key features and aspects of LoRaWAN :

* **Long Range** : LoRaWAN is designed to provide long-range communication, allowing devices to communicate over several kilometers in open environments. This makes it suitable for applications that require communication over a large geographic area.

* **Low Power** : Devices using LoRaWAN are typically low-power, which is crucial for IoT applications where devices may need to operate on battery power for extended periods. The low power consumption allows for long battery life.

* **Low Data Rates** : LoRaWAN is optimized for low data rate communication, making it suitable for IoT devices that transmit small amounts of data at regular intervals. This is different from high-bandwidth applications like video streaming.

* **Scalability** : LoRaWAN supports a scalable network architecture, allowing the deployment of a large number of devices within a network. It uses a star-of-stars topology, where end-devices communicate with gateways, and gateways forward the data to a central network server.

* **Secure Communication** : LoRaWAN incorporates security features to protect the communication between devices and the network. It uses encryption to secure data transmission and implements device authentication mechanisms.

* **Frequency Bands** : LoRaWAN operates in the unlicensed industrial, scientific, and medical (ISM) bands, with different frequency bands available in different regions. The most common frequency bands are 868 MHz in Europe, 915 MHz in North America, and 433 MHz in Asia. In india it is 865-867 MHz.

* **Open Standard** : LoRaWAN is an open standard, and its specifications are maintained by the LoRa Alliance, a non-profit association of companies supporting the development and adoption of the protocol.

* **Use Cases** : LoRaWAN is used in various applications, including smart agriculture, smart cities, asset tracking, industrial automation, and environmental monitoring. Its ability to cover long distances with low power consumption makes it suitable for applications where traditional connectivity options may be impractical or expensive.

## Device Activation Methods

***
In LoRaWAN, device activation methods refer to the process by which a device establishes a secure connection with the network server. There are two main activation methods: Activation by Personalization (ABP) and Over-The-Air Activation (OTA).
***

* **Activation By Personilization(ABP)**
    * In ABP, the device is pre-configured with specific parameters, including the Device Address, Network Session Key (NwkSKey), and Application Session Key (AppSKey).These parameters are manually provisioned into the device during manufacturing or before deployment.
    * ABP is relatively simpler and faster to set up because the device doesn't need to go through the process of exchanging messages with the network server to obtain keys dynamically.
    * However, ABP has some security considerations. Once the keys are provisioned, they remain static, and if compromised, the security of the device may be at risk.

* **Over the air activation (OTA)**
    * OTAA is a more secure and dynamic activation method. In this method, the device and the network server perform a handshake to exchange the necessary cryptographic keys during the activation process.
    * The device initially does not have a pre-configured Device Address or session keys. Instead, it sends a join request to the network server.
    * The network server responds with a join accept, containing the necessary keys and configuration parameters. The device then uses these keys for secure communication with the network server.
    * OTAA provides a higher level of security because the keys are not pre-configured, and the join procedure ensures that the device is authorized to join the network.

#### Key Differences between ABP and OTA
***
* **Security** : OTAA is considered more secure than ABP because it involves a dynamic key exchange during the activation process.
* **Ease of Deployment** : ABP is simpler and faster to deploy since devices come pre-configured with static keys. However, this simplicity comes with potential security trade-offs.
* **Key Management** : OTAA allows for more dynamic key management, making it easier to rotate keys periodically for improved security.

##### Note :
* The choice between ABP and OTAA depends on the specific requirements of the IoT application. 
* For applications where security is a top priority, or where devices need to be dynamically added or removed from the network, OTAA is often preferred. 
* For simpler deployments where security considerations may be less critical, ABP might be more suitable. 

***

## Device, Gateways and Application Layers : 
***
In a LoRaWAN (Long Range Wide Area Network) architecture, devices, gateways, and application layers play distinct roles in enabling communication for Internet of Things (IoT) devices. Let's explore each layer:

* **Device Layer** : 
    * Devices (End Devices or Nodes): These are the actual IoT devices that collect data from sensors or perform specific tasks. Examples include environmental sensors, smart meters, or tracking devices. LoRaWAN-enabled devices are designed to be low-power and are equipped with LoRa transceivers for long-range, low-power communication.

    * Functions : Devices are responsible for sensing or collecting data, encoding the data into LoRaWAN frames, and transmitting these frames to gateways. They also receive downlink messages from the network.

* **Gateway Layer** : 
    * Gateways: Gateways act as intermediaries between the end devices and the network server. They receive LoRaWAN frames from devices within their coverage range and forward these frames to the network server.
    * Functions: Gateways are responsible for receiving LoRaWAN transmissions from devices, demodulating the signals, and forwarding the data to the network server. They also receive downlink messages from the network server and transmit them to the appropriate devices.

* **Network Layer** :
    * Network Server: The network server manages the communication between devices and applications. It validates and decrypts messages from devices, routes the data to the correct application server, and manages the network's overall functionality.

    * Functions: The network server authenticates devices during the activation process, manages device sessions, handles the routing of messages to the appropriate application server, and implements security measures. It also manages the handshaking process during Over-The-Air Activation (OTAA).

* **Application Layer** :
    * Application Server: The application server is responsible for processing and storing data received from devices. It provides the interface between the IoT application and the LoRaWAN network.

    * Functions: The application server receives data from the network server, processes it, and forwards relevant information to the end-user application or cloud service. It may also send commands or downlink messages to devices through the network server.

#### Communication flow

* Devices transmit data to gateways.
* Gateways forward the data to the network server.
* The network server processes and routes the data to the appropriate application server.
* The application server processes the data and interacts with the end-user application or cloud service. 
* Downlink messages follow a reverse path: application server to network server to gateways to devices.

This layered architecture allows for efficient, long-range communication with low-power devices while providing a scalable and secure framework for IoT applications in various domains. Each layer plays a critical role in ensuring reliable and secure end-to-end communication.

***


